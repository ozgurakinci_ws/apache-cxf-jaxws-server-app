package ws_test;

/** 
* 	Author: ozgur.akinci
*	Created Date: 23.10.2019 14:15:00
*	Desc: 
**/

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

@WebService
public interface HelloWorld {

	String sayHi(@XmlElement(required=true) @WebParam(name = "param1") String param1) throws Exception;

}