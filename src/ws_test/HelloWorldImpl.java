package ws_test;

/** 
* 	Author: ozgur.akinci
*	Created Date: 23.10.2019 14:15:00
*	Desc: 
*	Link: http://localhost:8080/ASALWebServices/services/HelloWorldService?wsdl
**/

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

@WebService(endpointInterface = "ws_test.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	
	@Resource
	WebServiceContext wsc; 

	@Override
	public String sayHi(String param1) throws TestWSException{
		try {
			if(HelloWorldAuthenticate.authenticate(wsc))
				return "success";
			else
				return "username or password invalid.";
		} catch (Exception e) {
			throw new TestWSException("-1", e.getMessage());
		}
	}
}